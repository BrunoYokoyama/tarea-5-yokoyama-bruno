﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPrimerPromedio
{
    class Decorativo : Menaje
    {
        public enum Objeto
        {
            Vela,
            Florero,
        }
        public Objeto objeto;
        public Decorativo(string name,Objeto objeto, int cantidad) : base(name, cantidad)
        {
            this.objeto = objeto;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPrimerPromedio
{
    class Utensilio : Menaje
    {
        public enum Objeto
        {
            Cuchillo,
            Cuchara,
            Tenedor,
        }
        public Objeto objeto;
        public Utensilio(string name,Objeto objeto, int cantidad) : base(name, cantidad)
        {
            this.objeto = objeto;
        }
    }
}

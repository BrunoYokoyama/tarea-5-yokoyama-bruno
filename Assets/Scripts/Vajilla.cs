﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPrimerPromedio
{
    class Vajilla : Menaje
    {
        public enum Objeto
        {
           Tazon,
           Vaso,
           Bandeja,
           Jarra,

        }
        public Objeto objeto;
        public Vajilla(string name,Objeto objeto, int cantidad) : base(name, cantidad)
        {
            this.objeto=objeto;

            
        }
    }
}

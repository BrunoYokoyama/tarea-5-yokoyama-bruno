﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExamenPrimerPromedio;

public class NewBehaviourScript : MonoBehaviour
{
    static Menaje[] menajes;
    // Start is called before the first frame update
    void Start()
    {
        menajes = new Menaje[9];
        menajes[0] = new Decorativo("Floreros", Decorativo.Objeto.Florero, 20);
        menajes[1] = new Decorativo("Velas", Decorativo.Objeto.Vela, 42);
        menajes[2] = new Vajilla("Jarras", Vajilla.Objeto.Jarra, 29);
        menajes[3] = new Vajilla("Bandejas", Vajilla.Objeto.Bandeja, 12);
        menajes[4] = new Vajilla("Tazones", Vajilla.Objeto.Tazon, 79);
        menajes[5] = new Vajilla("Vasos", Vajilla.Objeto.Vaso, 21);
        menajes[6] = new Utensilio("Tenedores", Utensilio.Objeto.Tenedor, 4);
        menajes[7] = new Utensilio("Cuchillos", Utensilio.Objeto.Cuchillo, 7);
        menajes[8] = new Utensilio("Cucharas", Utensilio.Objeto.Cuchara, 25);

        Loop();
    }
     void Loop()
    {
        Debug.Log("\t" + "       Menajes");


        Debug.Log("\t" + "==== Decorativos =====");
     

        for (int i = 0; i < 2; i++)
        {
            Debug.Log("\t" + (1 + i) + " - " + menajes[i].name + " disponibles = " + menajes[i].cantidad);
        }
        
        Debug.Log("\t" + "====== Vajilla =======");
        
        for (int i = 2; i < 6; i++)
        {
            Debug.Log("\t" + (1 + i) + " - " + menajes[i].name + " disponibles = " + menajes[i].cantidad);
        }
       
        Debug.Log("\t" + "====== Utensilios =======");
       
        for (int i = 6; i < menajes.Length; i++)
        {
            Debug.Log("\t" + (1 + i) + " - " + menajes[i].name + " disponibles = " + menajes[i].cantidad);
        }
      // A que menaje se le quiere añadir cantidad
        Debug.Log("\t" + "Elija un menaje...");
        string str = "1";
        int opcion = int.Parse(str);

        // Cantidad que se va a añadir al menaje selecionado
        Debug.Log("\t " + "Cuanto desea añadir?...");
        string str2 = "10000";
        int modificacion = int.Parse(str2);

        menajes[opcion - 1].cantidad += modificacion;

       //El menaje con la cantidad modificada
        Debug.Log("\t " + "=====Final=====");
        Debug.Log("\t" + (opcion) + " - " + menajes[opcion - 1].name + " disponibles = " + menajes[opcion - 1].cantidad);
    }


}

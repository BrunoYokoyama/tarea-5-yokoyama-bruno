﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPrimerPromedio
{
    class Program
    {
        static Menaje[] menajes;
        
        static void Main(string[] args)
        {
            menajes = new Menaje[9];
            menajes[0] = new Decorativo("Floreros", Decorativo.Objeto.Florero, 20);
            menajes[1] = new Decorativo("Velas", Decorativo.Objeto.Vela, 42);
            menajes[2] = new Vajilla("Jarras", Vajilla.Objeto.Jarra, 29);
            menajes[3] = new Vajilla("Bandejas", Vajilla.Objeto.Bandeja, 12);
            menajes[4] = new Vajilla("Tazones", Vajilla.Objeto.Tazon, 79);
            menajes[5] = new Vajilla("Vasos", Vajilla.Objeto.Vaso, 21);
            menajes[6] = new Utensilio("Tenedores", Utensilio.Objeto.Tenedor, 4);
            menajes[7] = new Utensilio("Cuchillos", Utensilio.Objeto.Cuchillo, 7);
            menajes[8] = new Utensilio("Cucharas", Utensilio.Objeto.Cuchara, 25);

            Loop();
        }
        static void Loop()
        {
            Console.WriteLine("\t"+"       Menajes");
            

            Console.WriteLine("\t" + "==== Decorativos =====");
            Console.WriteLine(" ");

            for(int i = 0; i< 2; i++)
            {
                Console.WriteLine("\t"+ (1 + i)+" - "  + menajes[i].name + " disponibles = " + menajes[i].cantidad);
            }
            Console.WriteLine(" ");
            Console.WriteLine("\t"+"====== Vajilla =======");
            Console.WriteLine(" ");
            for (int i = 2; i< 6; i++)
            {
                Console.WriteLine("\t"+ (1 + i)+" - "  + menajes[i].name + " disponibles = " + menajes[i].cantidad);
            }
            Console.WriteLine(" ");
            Console.WriteLine("\t" + "====== Utensilios =======");
            Console.WriteLine(" ");
            for (int i = 6; i < menajes.Length; i++)
            {
                Console.WriteLine("\t" + (1 + i) + " - " + menajes[i].name + " disponibles = " + menajes[i].cantidad);
            }
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("\t" + "Elija un menaje...");
            string str = Console.ReadLine();
            int opcion = Int32.Parse(str);

            
            Console.WriteLine("\t "+"Cuanto desea añadir?...");
            string str2 = Console.ReadLine();
            int modificacion = Int32.Parse(str2);

            menajes[opcion - 1].cantidad += modificacion;
            
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("\t " + "=====Final=====");
            Console.WriteLine("\t" + (opcion) + " - " + menajes[opcion-1].name + " disponibles = " + menajes[opcion-1].cantidad);
        }
    }
}
